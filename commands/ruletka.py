import datetime
from random import choice
from base import BaseCommand


class Ruletka(BaseCommand):
    command = 'ruletka'
    roulette = [
        'Strzał',
        'Talon na kurwe i balon xD',
        'Voucher na :fist::ing: o wartości $300',
        'Podpierdolenie przez warszawkę',
        'Zgwałcenie przez murzyna',
        'Milion dolarów',
        'Darmowe owoce w firmie (tylko co drugi poniedziałek)',
        ':printer:',
        'Kupon upoważniający do nasrania sobie do mordy o ujemnej wartości',
        'Grzechów odpuszczenie',
        'Żywot wieczny amen',
        'Ser, który smakuje jak miód, gdy się dużo je :lizard:',
    ]

    def update_user_date(self, user):
        with open('./data/ruletka-%s' % user, 'w') as fhn:
            fhn.write(datetime.date.today().isoformat())

    def check_user(self, user):
        try:
            with open('./data/ruletka-%s' % user, 'r') as fhn:
                stored_date = fhn.read()
                if stored_date == datetime.date.today().isoformat():
                    return False
                self.update_user_date(user)
                return True
        except FileNotFoundError:
            self.update_user_date(user)
            return True

    def handle_command(self, command, channel, evt):
        user = evt['user']
        if self.check_user(user):
            return '<@%s> %s' % (user, choice(self.roulette))

        return '<@{}> Spierdalaj - dziś już grałeś.'.format(user)
