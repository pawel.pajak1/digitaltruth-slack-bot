
import re
from datetime import timedelta
from random import choice

import requests
from bs4 import BeautifulSoup

from base import BaseCommand

WS_RE = re.compile('\s{2,}')

def float_to_minutes(value):
    try:
        minutes_float = float(value)
    except ValueError:
        return value
    ret = str(timedelta(minutes=minutes_float))
    if minutes_float < 60:
        return ret[2:]
    return ret

def format_time_cell(value):
    try:
        delim = '-'
        if '+' in value:
            delim = '+'
        times = value.split(delim)
        kurwa = []
        for ts in times:
            #kurwa.append(('%02.2f' % float(ts)).rjust(5, '0'))
            kurwa.append(float_to_minutes(ts))
        return delim.join(kurwa)
    except ValueError:
        return value

class DigitaltruthCommand(BaseCommand):
    command = 'times for'
    no_results = [
        'Chuj mi w ryj',
        'Pierdole',
        'Gówno',
        'Sranie do ryja',
        'Wymocz se to w sikach xD',
        'Nasraj sobie do ryja',
        'Poszukaj w internecie xD',
        'Skontaktuj się ze sprzedawcą i nasraj se do mordy xD',
        'Pomódl się do Jana Wywoływacza II, wielkiego pedofila',
        'Ja bym polecał caffenol',
        'Nie jestem wróżką, ani tym bardziej kurwą. Wypierdalaj śmieciu biedaku i kup se jakiś normalny film a nie jakieś gówno xD'
    ]

    def get_note_text(self, url):
        resp = requests.get(url)
        doc = BeautifulSoup(resp.content, "lxml")
        table = doc.find_all('table', {'class': 'notenote'})
        rows = table[0].find_all('tr')
        ret_arr = []
        for row in rows:
            for td in row.find_all('td'):
                txt = td.text.strip()
                if len(txt) > 2:
                    ret_arr.append(txt)
        return ', '.join(ret_arr)

    def get_notes(self, cell):
        finds = cell.find_all('a')
        if not finds:
            return ''
        else:
            return self.get_note_text('https://digitaltruth.com/' + finds[0]['href'])

    def _get_film_name(self, q_arr):
        if not 'in' in q_arr and not 'at' in q_arr:
            return ' '.join(q_arr)
        
        stops = []
        if 'in' in q_arr:
            stops.append(q_arr.index('in'))
        if 'at' in q_arr:
            stops.append(q_arr.index('at'))
        first_stop = min(stops)
        return " ".join(q_arr[0:first_stop])

    def _get_q_param(self, query, param):
        query = re.sub(WS_RE, ' ', query)
        q_arr = query.split(' ')
        if param == 'film':
            return self._get_film_name(q_arr)
        if not param in q_arr:
            return ''
        idx = q_arr.index(param)
        return q_arr[idx + 1]

    def digitaltruth_search(self, query):
        film = self._get_q_param(query, 'film')
        dev = self._get_q_param(query, 'in')
        iso = self._get_q_param(query, 'at')
        params = {
            'Film': '%' + film + '%',
            'Developer': '%' + dev + '%',
            'mdc': 'Search',
            'TempUnits': 'C',
            'TimeUnits': 'D'
        }
        
        resp = requests.get('https://digitaltruth.com/devchart.php', params=params)

        try:
            doc = BeautifulSoup(resp.content, "lxml")
            tab = doc.find_all('table', {'class': "mdctable"})[0]

            resp = []
            rows = tab.find_all('tr')
        except IndexError:
            return []

        for row in rows[1:]:
            cells = row.find_all('td')
            if iso and iso != cells[3].text:
                continue
            dat = {
                'film': cells[0].text,
                'dev': cells[1].text,
                'dil': cells[2].text,
                'EI': cells[3].text,
                'time': cells[4].text,
                'temp': cells[7].text,
                'notes': self.get_notes(cells[8]),
                'time_fmt': format_time_cell(cells[4].text)
            }
            resp.append(dat)
        return resp

    def handle_command(self, command, channel, evt):
        """
        Executes bot command if the command is known
        """

        response = None
        command = command.replace(self.command + ' ', '')

        try:
            response = ""
            resp_data = self.digitaltruth_search(command)

            if not resp_data:
                response = "Brak danych w badzie cyfrowej gównoprawdy. %s" % choice(self.no_results)
            else:
                for item in resp_data:
                    response += "*%(film)s in %(dev)s*\t(%(dil)s) at %(EI)s:\t:clock3: *%(time_fmt)s*\t:thermometer: %(temp)s\t %(notes)s\n" % item
        except Exception as e:
            response = "Spierdolilo sie cos %s" % e
            #raise(e)
        return response
