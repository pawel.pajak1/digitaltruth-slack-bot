import io
import os
from random import choice

import requests
from PIL import Image

from base import BaseMessageParser

LEFT = 553
TOP = 466

WIDTH = 244
HEIGHT = 139


def download_image(url):
    headers = {
        'Authorization': 'Bearer %s' % os.environ.get('SLACKBOT_TOKEN')
    }
    r = requests.get(url, timeout=4.0, headers=headers)
    out = io.BytesIO(r.content)
    out.seek(0)
    img = Image.open(out)
    return img


class SpodekXD(BaseMessageParser):
    def __init__(self):
        self.slack_cli = None

    def handle_message(self, event, bot_id):
        if event.get('user', '') == bot_id:
            return

        if 'files' not in event:
            return

        message = event.get('text', '')
        if not message.startswith('spodek'):
            return
        spodek = Image.open('data/spodek.png')
        url = event['files'][0]['url_private_download']
        mime = event['files'][0]['mimetype']
        channel = event['channel']

        img = download_image(url)
        img = img.resize((WIDTH, HEIGHT), Image.ANTIALIAS)
        spodek.paste(img, (LEFT, TOP))

        out = io.BytesIO()
        spodek.save(out, "JPEG", quality=90)
        out.seek(0)
        resp = self.slack_cli.api_call(
            "files.upload",
            channels=[channel],
            file=out,
            title="hehehe"
        )
        out.close()
        del img
        del spodek
